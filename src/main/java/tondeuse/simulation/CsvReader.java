package tondeuse.simulation;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

class CsvReader {

    private static final String SEPARATOR = " ";

    protected List<Integer> readLine(File file) {
        List<Integer> list = new ArrayList<>();
        try (InputStream inputFS = new FileInputStream(file);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputFS))) {
            list.addAll(reader.lines()
                    .findFirst()
                    .map(line -> Arrays.stream(line.split(SEPARATOR)).mapToInt(Integer::parseInt).boxed()
                            .collect(Collectors.toList())).get()
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;

    }


    protected List<List<String>> readRecords(File file) {
        try (InputStream inputFS = new FileInputStream(file);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputFS))) {
            return reader.lines()
                    .skip(1)
                    .map(line -> {

                        if (line.trim().indexOf(" ") > 0 && !isEmpty(Arrays.asList(line.trim().split("\\s+")))) {
                            return Arrays.asList(line.split(" "));
                        } else {
                            return line.chars().mapToObj(x->(char)x).map(x->x.toString()).collect(Collectors.toList());
                        }
                    }).collect(Collectors.toList());

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


}