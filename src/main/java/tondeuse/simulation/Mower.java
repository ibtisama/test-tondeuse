package tondeuse.simulation;

import java.util.ArrayList;
import java.util.List;

import static tondeuse.simulation.Direction.*;

public class Mower {
    private Coordinate coordinate;
    private List<String> instructions = new ArrayList<>();
    private static final String LEFT = "G";

    public Mower(Coordinate coordinate) {
        this.coordinate = coordinate;
    }


    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public List<String> getInstructions() {

        return instructions == null ? new ArrayList<>() : instructions;

    }

    public void setInstructions(List<String> instructions) {

        this.instructions = instructions;
    }

    public Mower executeInstructions(Coordinate maxCoordinates) {
        String newCardinal;
        for (String instruction : instructions) {
            switch (instruction) {
                case "D":
                    newCardinal = changeDirection(coordinate.getCardinal(), "D");
                    this.coordinate.setCardinal(newCardinal);
                    break;
                case "G":
                    newCardinal = changeDirection(coordinate.getCardinal(), "G");
                    this.coordinate.setCardinal(newCardinal);
                    break;
                case "A":
                    move(maxCoordinates);
                    break;
            }
        }

        return this;
    }

    private String changeDirection(String actualCardinal, String direction) {

        Direction cardinal = find(actualCardinal);
        if (LEFT.equalsIgnoreCase(direction)) {
            return cardinal.rotateLeft().getValues().get(1);
        } else {
            return cardinal.rotateRight().getValues().get(1);
        }
    }

    private void move(Coordinate maxCoordinate) {
        Direction direction = find(coordinate.getCardinal());
             switch (direction) {
                case NORTH:
                    if(coordinate.getY() < maxCoordinate.getY() )
                    this.setCoordinate(new Coordinate(coordinate.getX(), coordinate.getY() + Integer.parseInt(NORTH.getValues().get(2)), coordinate.getCardinal()));
                    break;
                case SOUTH:
                    if(coordinate.getY() > 0 )
                        this.setCoordinate(new Coordinate(coordinate.getX(), coordinate.getY() + Integer.parseInt(SOUTH.getValues().get(2)), coordinate.getCardinal()));
                    break;
                case WEST:
                    if(coordinate.getX() > 0)
                        this.setCoordinate(new Coordinate(coordinate.getX() + Integer.parseInt(WEST.getValues().get(2)), coordinate.getY(), coordinate.getCardinal()));
                    break;
                case EAST:
                    if(coordinate.getX() < maxCoordinate.getX())
                        this.setCoordinate(new Coordinate(coordinate.getX() + Integer.parseInt(EAST.getValues().get(2)), coordinate.getY(), coordinate.getCardinal()));
                    break;

        }

    }


}

