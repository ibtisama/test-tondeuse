package tondeuse.simulation;


import java.util.Arrays;
import java.util.List;

public enum Direction {
    NORTH("0", "N", "1")  ,
    EAST("90", "E","1")  ,
    SOUTH("180", "S", "-1")  ,
    WEST("270", "W", "-1")  ;

    private final List<String> values;

    private Direction(String ...values) {
        this.values = Arrays.asList(values);
    }

    public List<String> getValues() {
        return values;
    }

    public static Direction find(String name) {
        for (Direction direction : Direction.values()) {
            if (direction.getValues().contains(name)) {
                return direction;
            }
        }
        return null;
    }



    public static Direction get(final int degrees) {
        int ordinal = ((degrees % 360) / 90) - ( 4 * (degrees % 90));
        return ordinal < 0 ?  null : values()[ordinal];
    }
    public Direction rotateRight() {
        return Direction.get((Integer.parseInt(this.getValues().get(0)) + 90) % 360);
    }

    public Direction rotateLeft() {
        return Direction.get((Integer.parseInt(this.getValues().get(0)) + 270) % 360);
    }

}