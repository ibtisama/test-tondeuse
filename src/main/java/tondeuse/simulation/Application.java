package tondeuse.simulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.lang.Integer.valueOf;


@SpringBootApplication
public class Application implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(String... args) {

        Path path = null;
        try {
            path = Paths.get(ClassLoader.getSystemResource("entries.csv").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        File file = new File(path.toString());

        CsvReader csvReader = new CsvReader();
        final Coordinate gridTopCoordinate = new Coordinate(csvReader.readLine(file).get(0), csvReader.readLine(file).get(1), "");

        List<Mower> mowers = new ArrayList<>();
        List<List<String>> listOfRecords = csvReader.readRecords(file);
        while (listOfRecords.size() > 0) {
            List<String> coordinates = listOfRecords.get(0);
            Coordinate coordinate = new Coordinate(valueOf(coordinates.get(0)), valueOf(coordinates.get(1)), coordinates.get(2));
            Mower mower = new Mower(coordinate);
            mower.getInstructions().addAll(listOfRecords.get(1));
            mowers.add(mower);
            if (listOfRecords.size() == 2) {
                break;
            } else {
                listOfRecords.subList(0,2).clear();
            }
        }

        int index = 0;
        while (index < mowers.size()) {

            int finalIndex = index;
            CompletableFuture<Mower> completableFuture = CompletableFuture.supplyAsync(() -> mowers.get(finalIndex).executeInstructions(gridTopCoordinate));
            while (!completableFuture.isDone()) {
                System.out.println("mower[" + index + "] is still moving please wait...");
            }

            try {
                Coordinate newCoordinate = completableFuture.get().getCoordinate();
                System.out.println("**********************************************************************");
                System.out.println(" New mower[" + index + "] coordinate is ---------> " + newCoordinate);
                System.out.println("***********************************************************************");
                index++;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }


    }


}



